import { StyleSheet } from 'react-native';

import Constants from 'expo-constants';

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        paddingTop: Constants.statusBarHeight + 20,//Pega o tamanho da status bar do dispositivo e coloca 20px no topo
    },

    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    headerText: {
        fontSize: 16,
        color: '#737380',
    },

    headerTextBold: {
        fontWeight: 'bold'
    },

    title: {
        fontSize: 30,
        color: '#13131a',
        marginBottom: 16,
        marginTop: 48,
        fontWeight: 'bold'
    },
    
    description: {
        fontSize: 16,
        color: '#737380',
        lineHeight: 24,
    },

    incidentList: {
        marginTop: 32,
    },

    incident: {
        padding: 24,
        backgroundColor: "#fff",
        marginBottom: 16,
        borderRadius: 8,
    },

    incidentProperty: {
        fontSize: 14,
        color: '#41414d',
        fontWeight: 'bold',
    },

    incidentValue: {
        marginTop: 8,
        fontSize: 15,
        marginBottom: 24,
        color: '#737380',
    },

    detailsButton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },

    detailsButtonText: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#e02140',
    },
});