const express = require('express');

const OngController = require('./controllers/OngController');
const IncidentController = require('./controllers/IncidentController');
const ProfileController = require('./controllers/ProfileController');
const SessionController = require('./controllers/SessionController');

const routes = express.Router();

//Verificar se a Ong existe
routes.post('/sessions', SessionController.create);

//Listagem e criação das Ongs
routes.get('/ongs', OngController.index);
routes.post('/ongs', OngController.create);

//Listar casos de uma Ong
routes.get('/profile', ProfileController.index);

//Listagem e criação dos casos
routes.get('/incidents', IncidentController.index);
routes.post('/incidents', IncidentController.create);
routes.delete('/incidents/:id', IncidentController.delete);

module.exports = routes;