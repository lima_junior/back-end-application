import React from 'react';

//importação do css global
import './global.css';

//Importação do componente Header
import Route from './routes';


function App() {
  return (
    <Route />    
  );
}

export default App;
