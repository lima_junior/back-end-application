import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi';

//importação do arquivo de chamada da api
import api from '../../services/api';

import './styles.css';

import logoImg from '../../assets/logo.svg';

export default function Register() {

    const [ name, setName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ whatsapp, setWhatsapp ] = useState('');
    const [ city, setCity ] = useState('');
    const [ uf, setUf ] = useState('');

    //Função responsável por fazer o cadastro do usuário
    /*
     * Para que a função seja acionada logo após o evento de clique no botão
     * é preciso incluir o método onSubmit no formulário.
     */

     /*
     * Para prevenir o recarregamento da página ao clicar no botão podemos fazer a função receber o evento
     * de clique 'e' na função e adiconar um .preventDefault();
     */

    const history = useHistory();

    async function handleRegister(e) {
        e.preventDefault();
        // Variável guardando o array que será enviado para o serviço web que cadastra a ONG 
        const data = {
            name,
            email,
            whatsapp,
            city,
            uf
        };

        try {
            // Enviar a requisição
            const response = await api.post('ongs', data);
            
            alert(`Seu ID de acesso: ${ response.data.id }`);

            history.push('/');

        } catch (error) {
            alert(`Erro no cadastro, tente novamente.`);
        }
        
    }

    return (
        <div className="register-container">
            <div className="content">
                <section>
                    <img src={ logoImg } alt="Be The Hero" />

                    <h1>Cadastro</h1>
                    <p>Faça seu cadastro, entre na plataforma e ajude as pessoas a encontrarem os casos da sua ONG.</p>
                    
                    <Link className="back-link" to="/">
                        <FiArrowLeft size={16} color="#E02041"/>
                        Voltar para o logon
                    </Link>

                </section>

                <form onSubmit={ handleRegister }>
                    <input 
                        placeholder="Nome da ONG" 
                        value={ name }
                        onChange={ e => setName(e.target.value) }
                    />
                    <input 
                        type="email"  
                        placeholder="Email" 
                        value={ email }
                        onChange={ e => setEmail(e.target.value) }
                    />
                    <input 
                        placeholder="Whatsapp" 
                        value={ whatsapp }
                        onChange={ e => setWhatsapp(e.target.value) }
                    />

                    <div className="input-group">
                        <input 
                            placeholder="Cidade" 
                            value={ city }
                            onChange={ e => setCity(e.target.value) }
                        />
                        <input 
                            placeholder="UF" 
                            style={{ width:80 }} 
                            value={ uf }
                            onChange={ e => setUf(e.target.value) }
                        />
                    </div>

                    <button className="button" type="submit">Cadastrar</button>
                </form>    
            </div>
        </div>
    );
}