import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:3333',
});

export default api; // O export default serve para permitir que os outros arquivos possam importar esse arquivo